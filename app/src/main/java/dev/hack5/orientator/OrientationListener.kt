/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.content.Context
import android.graphics.Point
import android.hardware.display.DisplayManager
import android.os.Build
import android.util.Log
import android.util.Size
import android.view.Display
import android.view.OrientationEventListener
import android.view.Surface
import android.view.WindowManager
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*


data class ClickerState(val clickerVisibility: Boolean, val customRotation: Int, val customClickerOffset: Point, val clickerSize: Size, val hideDelay: Long)


@OptIn(DelicateCoroutinesApi::class)
class OrientationListener(private val context: Context, private val windowManager: WindowManager, private val displayManager: DisplayManager) : OrientationEventListener(context) {
    private var currentRotation: Int? = null
    private var currentOrientation: Int? = null
    private var clickerView: ClickerOverlayView? = null
    private var rotationState: RotationState = RotationState.ALL
    private val validTarget = MutableSharedFlow<Boolean>()
    private var targetRotation: Int? = null
    private val clickerVisibility = MutableStateFlow(false)
    private val clickerHidden = MutableStateFlow(false)
    private val debounceInterval = context.settingsData.data.map { it[DEBOUNCE_INTERVAL] ?: 300L }

    init {
        GlobalScope.launch {
            debounceInterval.collectLatest { debounceInterval ->
                // reset state when debounce interval changes
                clickerVisibility.value = false
                clickerHidden.value = false

                coroutineScope {
                    var waiterTask: Job? = null
                    var lastValid = 0L
                    combine(validTarget, clickerHidden, ::Pair).collect { (validity, hidden) ->
                        if (validity) {
                            if (!hidden) {
                                lastValid = System.nanoTime()
                            }
                            waiterTask?.cancel()
                            if (!hidden) {
                                clickerVisibility.value = true
                            }
                        } else {
                            val duration = debounceInterval - (System.nanoTime() - lastValid) / 1000000L
                            waiterTask?.cancel()
                            waiterTask = launch {
                                delay(duration)
                                clickerVisibility.value = false
                                clickerHidden.value = false
                            }
                        }
                    }
                }
            }

        }

        GlobalScope.launch {
            var lastCustomRotation: Int? = null
            var lastCustomOffset: Point? = null
            var lastClickerSize: Size? = null
            combine(clickerVisibility, context.settingsData.data) { clickerVisibility, preferences ->
                ClickerState(
                    clickerVisibility,
                    preferences[CUSTOM_ROTATION] ?: 0,
                    Point(
                        preferences[CUSTOM_CLICKER_OFFSET_X] ?: 0,
                        preferences[CUSTOM_CLICKER_OFFSET_Y] ?: 0
                    ),
                    Size(
                        preferences[CUSTOM_CLICKER_DIAMETER] ?: context.resources.getDimensionPixelSize(R.dimen.overlay_width),
                        preferences[CUSTOM_CLICKER_DIAMETER] ?: context.resources.getDimensionPixelSize(R.dimen.overlay_height)
                    ),
                    preferences[HIDE_DELAY] ?: 0
                )
            }.collectLatest { (clickerVisibility, customRotation, customOffset, clickerSize, hideDelay) ->
                var animate = true
                var remove = false
                if (customOffset != lastCustomOffset) {
                    lastCustomOffset = customOffset
                    animate = false
                    remove = true
                }
                if (lastClickerSize != clickerSize) {
                    lastClickerSize = clickerSize
                    animate = false
                    remove = true
                }
                if (customRotation != lastCustomRotation) {
                    lastCustomRotation = customRotation
                    animate = true // overriding customOffset
                    remove = true
                }

                withContext(Dispatchers.Main) {
                    if (clickerVisibility) {
                        if (remove) {
                            removeClickerOverlay(animate)
                        }
                        addClickerOverlay(customRotation, customOffset, clickerSize, animate)

                        if (hideDelay > 0L) {
                            launch {
                                delay(hideDelay)
                                this@OrientationListener.clickerVisibility.value = false
                                clickerHidden.value = true
                            }
                        }
                    } else {
                        removeClickerOverlay(true)
                    }
                }
            }
        }
    }

    fun onConfigurationChanged() {
        currentRotation = getRotationSetting(context, displayManager)
        currentOrientation?.let { GlobalScope.launch { recalculate(it, currentRotation!!, rotationState) } }
    }

    override fun onOrientationChanged(orientation: Int) {
        if (orientation == ORIENTATION_UNKNOWN) {
            currentOrientation = null
            GlobalScope.launch {
                validTarget.emit(false)
            }
            return
        }
        currentOrientation = orientation
        currentRotation?.let { GlobalScope.launch { recalculate(orientation, it, rotationState) } }
    }

    fun updateRotationState(newState: RotationState) {
        rotationState = newState
        currentRotation?.let { rotation ->
            currentOrientation?.let { orientation ->
                GlobalScope.launch { recalculate(orientation, rotation, rotationState) }
            }
        }
    }

    private suspend fun recalculate(orientation: Int, currentRotation: Int, rotationState: RotationState) {
        targetRotation = when (orientation.mod(360)) {
            in 45 until 135 -> Surface.ROTATION_270
            in 135 until 225 -> Surface.ROTATION_180
            in 225 until 315 -> Surface.ROTATION_90
            else /* in 315 until 360, in 0 until 45 */ -> Surface.ROTATION_0
        }
        Log.v(TAG, "New rotation $targetRotation ($orientation°), was $currentRotation in state $rotationState")
        validTarget.emit(if (targetRotation != currentRotation) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val displayMode = displayManager.getDisplay(Display.DEFAULT_DISPLAY).mode
                val naturallyLand = displayMode.physicalWidth > displayMode.physicalHeight
                val oddRotation =
                    targetRotation == Surface.ROTATION_90 || targetRotation == Surface.ROTATION_270
                when (rotationState) {
                    RotationState.ALL -> true
                    RotationState.LANDSCAPE_ONLY -> oddRotation xor naturallyLand
                    RotationState.PORTRAIT_ONLY -> !(oddRotation xor naturallyLand)
                    RotationState.NONE -> false
                }
            } else {
                true // not supported below M
            }
        } else {
            false
        })
    }

    private fun addClickerOverlay(customRotation: Int, customOffset: Point, clickerSize: Size, animate: Boolean) {
        if (clickerView == null) {
            clickerView = addClickerOverlay(context, windowManager, displayManager, customRotation, customOffset, clickerSize, animate).also {
                it.setOnClickListener {
                    removeClickerOverlay(false)
                    setRotation(context, targetRotation!!)
                    currentRotation = targetRotation!!
                }
            }
        }
    }

    private fun removeClickerOverlay(animate: Boolean) {
        clickerView?.let {
            removeOverlay(it, windowManager, animate)
            clickerView = null
        }
    }
}

private const val TAG = "OrientationListener"
