/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.graphics.Insets
import android.graphics.PixelFormat
import android.graphics.Point
import android.graphics.PorterDuff
import android.hardware.display.DisplayManager
import android.os.Build
import android.util.Log
import android.util.Size
import android.view.*
import android.view.WindowManager.LayoutParams.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.LinearInterpolator
import androidx.annotation.RequiresApi
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.animation.addListener
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.shape.ShapeAppearanceModel
import kotlin.math.max

fun getLayoutParams(gravityLocationSize: Triple<Int, Point, Size>?): WindowManager.LayoutParams {
    val layoutParams = WindowManager.LayoutParams()
    layoutParams.type = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) TYPE_APPLICATION_OVERLAY else @Suppress("DEPRECATION") TYPE_SYSTEM_OVERLAY
    if (gravityLocationSize != null) {
        val (gravity, location, size) = gravityLocationSize

        layoutParams.flags = FLAG_NOT_FOCUSABLE
        layoutParams.format = PixelFormat.RGBA_8888
        layoutParams.width = size.width
        layoutParams.height = size.height
        layoutParams.gravity = gravity
        layoutParams.x = location.x
        layoutParams.y = location.y
    } else {
        layoutParams.flags = FLAG_NOT_FOCUSABLE or FLAG_NOT_TOUCHABLE
        layoutParams.format = PixelFormat.TRANSPARENT
        layoutParams.width = 0
        layoutParams.height = 0
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        layoutParams.fitInsetsSides = 0
        layoutParams.fitInsetsTypes = 0
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        layoutParams.layoutInDisplayCutoutMode =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) LAYOUT_IN_DISPLAY_CUTOUT_MODE_ALWAYS else LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
    }
    layoutParams.rotationAnimation = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ROTATION_ANIMATION_SEAMLESS else ROTATION_ANIMATION_JUMPCUT

    return layoutParams
}

fun addClickerOverlay(context: Context, windowManager: WindowManager, displayManager: DisplayManager, customRotation: Int, customOffset: Point, clickerSize: Size, animateEntry: Boolean): ClickerOverlayView {
    val view = ClickerOverlayView(ContextThemeWrapper(context, R.style.Theme_Orientator), windowManager, displayManager, customRotation, customOffset, clickerSize, animateEntry)
    val layoutParams = getLayoutParams(null)

    windowManager.addView(view, layoutParams)
    return view
}

fun updateClickerOverlay(context: Context, view: ClickerOverlayView, windowManager: WindowManager, gravity: Int, x: Int, y: Int, size: Size, animateEntry: Boolean) {
    val layoutParams = getLayoutParams(Triple(gravity, Point(x, y), size))

    if (animateEntry && view.isAttachedToWindow) {
        val radius = layoutParams.width / 2
        val circularReveal = ViewAnimationUtils.createCircularReveal(view, radius, radius, 0f, radius.toFloat())
        val rotate = ObjectAnimator.ofFloat(view, "rotation", 0f, 180f)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            rotate.interpolator = AccelerateDecelerateInterpolator()
        } else {
            rotate.interpolator = LinearInterpolator()
        }
        view.visibility = View.INVISIBLE
        windowManager.updateViewLayout(view, layoutParams)
        AnimatorSet().also {
            it.playTogether(rotate, circularReveal)
            it.start()
        }
    }
    view.visibility = View.VISIBLE
}

fun addTrackerOverlay(context: Context, windowManager: WindowManager, tracker: OrientationListener): TrackerOverlayView {
    val view = TrackerOverlayView(context, tracker)
    val layoutParams = getLayoutParams(null)

    windowManager.addView(view, layoutParams)
    return view
}

fun removeOverlay(view: View, windowManager: WindowManager, animate: Boolean) {
    if (animate && view.isAttachedToWindow) {
        val radius = view.width / 2
        val circularReveal = ViewAnimationUtils.createCircularReveal(view, radius, radius, radius.toFloat(), 0f)
        circularReveal.addListener(onEnd = {
            view.visibility = View.INVISIBLE
            windowManager.removeView(view)
        })
        val rotate = ObjectAnimator.ofFloat(view, "rotation", 0f, 180f)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            rotate.interpolator = AccelerateDecelerateInterpolator()
        } else {
            rotate.interpolator = LinearInterpolator()
        }
        AnimatorSet().also {
            it.playTogether(rotate, circularReveal)
            it.start()
        }
    } else {
        windowManager.removeView(view)
    }
}



class ClickerOverlayView(context: Context, private val windowManager: WindowManager?, private val displayManager: DisplayManager?, private val customRotation: Int, private val customOffset: Point, private val clickerSize: Size, animateEntry: Boolean) : ShapeableImageView(context) {
    constructor(context: Context) : this(context, null, null, 0, Point(0, 0), Size(100, 100), false)
    private var visible = !animateEntry

    init {
        assert(windowManager != null || isInEditMode)
        assert(displayManager != null || isInEditMode)
        setImageDrawable(AppCompatResources.getDrawable(context, R.drawable.ic_rotate_button))
        shapeAppearanceModel = ShapeAppearanceModel.builder().setAllCornerSizes(ShapeAppearanceModel.PILL).build()
        setBackgroundColor(ContextCompat.getColor(context, R.color.overlay_background))
        ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(ContextCompat.getColor(context, R.color.overlay_foreground)))
        imageTintMode = PorterDuff.Mode.MULTIPLY
        val xPadding = clickerSize.width / 6
        val yPadding = clickerSize.height / 6
        setContentPadding(xPadding, yPadding, xPadding, yPadding)
    }

    override fun onApplyWindowInsets(insets: WindowInsets?): WindowInsets {
        val hardInsets = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                windowManager!!.maximumWindowMetrics.windowInsets.displayCutout
            }
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.P -> {
                insets!!.displayCutout
            }
            else -> {
                null
            }
        }

        val maxSoftInset = if (insets != null) {
            (when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                    val statusBars = windowManager!!.maximumWindowMetrics.windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.statusBars())
                    val navBars = windowManager.maximumWindowMetrics.windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.navigationBars())
                    intArrayOf(
                        max(statusBars.bottom, navBars.bottom - (hardInsets?.safeInsetBottom ?: 0)),
                        max(statusBars.left, navBars.left - (hardInsets?.safeInsetLeft ?: 0)),
                        max(statusBars.right, navBars.right - (hardInsets?.safeInsetRight ?: 0)),
                        max(statusBars.top, navBars.top - (hardInsets?.safeInsetTop ?: 0))
                    )

                }
                Build.VERSION.SDK_INT == Build.VERSION_CODES.Q -> {
                    @Suppress("DEPRECATION")
                    insets.stableInsets.toArray()
                }
                else -> {
                    @Suppress("DEPRECATION")
                    intArrayOf(insets.stableInsetBottom, insets.stableInsetLeft, insets.stableInsetRight, insets.stableInsetTop)
                }
            }).maxOrNull()!!
        } else {
            0
        }

        var xInset = maxSoftInset
        var yInset = maxSoftInset

        val rawRotation = when (displayManager!!.getDisplay(Display.DEFAULT_DISPLAY).rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 1
            Surface.ROTATION_180 -> 2
            Surface.ROTATION_270 -> 3
            else -> {
                Log.e(TAG, "Unknown rawRotation $rotation")
                0
            }
        }

        when (rawRotation) {
            0 -> {
                xInset += customOffset.x
                yInset += customOffset.y
            }
            1 -> {
                xInset += customOffset.y
                yInset += customOffset.x
            }
            2 -> {
                xInset += customOffset.x
                yInset += customOffset.y
            }
            3 -> {
                xInset += customOffset.y
                yInset += customOffset.x
            }
        }

        val rotation = (rawRotation + customRotation).rem(4)
        if (null != hardInsets && Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            when (rotation) {
                // TODO smarter inset avoidance - find closest point that isn't in an inset
                0 -> {
                    xInset += hardInsets.safeInsetLeft
                    yInset += hardInsets.safeInsetBottom
                }
                1 -> {
                    xInset += hardInsets.safeInsetRight
                    yInset += hardInsets.safeInsetBottom
                }
                2 -> {
                    xInset += hardInsets.safeInsetRight
                    yInset += hardInsets.safeInsetTop
                }
                3 -> {
                    xInset += hardInsets.safeInsetLeft
                    yInset += hardInsets.safeInsetTop
                }
            }
        }

        val gravity = when (rotation) {
            Surface.ROTATION_0 -> Gravity.BOTTOM or Gravity.START
            Surface.ROTATION_90 -> Gravity.BOTTOM or Gravity.END
            Surface.ROTATION_180 -> Gravity.TOP or Gravity.END
            Surface.ROTATION_270 -> Gravity.TOP or Gravity.START
            else -> {
                Log.e(TAG, "Unknown rotation $rotation")
                Gravity.BOTTOM or Gravity.START
            }
        }

        updateClickerOverlay(context, this, windowManager!!, gravity, xInset, yInset, clickerSize, !visible)
        visible = true


        return super.onApplyWindowInsets(insets)
    }

    override fun isImportantForAccessibility(): Boolean {
        return false
    }
}

class TrackerOverlayView(context: Context, private val tracker: OrientationListener?) : View(context) {
    constructor(context: Context) : this(context, null)
    init {
        assert(tracker != null || isInEditMode)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        tracker?.onConfigurationChanged()
        super.onConfigurationChanged(newConfig)
    }

    override fun isImportantForAccessibility(): Boolean {
        return false
    }
}

@RequiresApi(Build.VERSION_CODES.Q)
private fun Insets.toArray(): IntArray {
    return intArrayOf(bottom, left, right, top)
}

private const val TAG = "Overlays"