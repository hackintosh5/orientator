/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_SCREEN_OFF
import android.content.Intent.ACTION_SCREEN_ON
import android.content.IntentFilter
import android.hardware.display.DisplayManager
import android.os.Binder
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import android.view.WindowManager
import androidx.core.content.getSystemService

class OrientationTrackerService : Service() {
    private var orientationListener: OrientationListener? = null
    private var trackerView: TrackerOverlayView? = null
    private var windowManager: WindowManager? = null

    private val screenStateReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                ACTION_SCREEN_ON -> {
                    orientationListener?.enable()
                }
                ACTION_SCREEN_OFF -> {
                    orientationListener?.disable()
                }
            }
        }
    }
    private var isReceiverRegistered = false

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onCreate() {
        if (
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            && !(Settings.System.canWrite(this) && Settings.canDrawOverlays(this))
        ) {
            stopSelf()
            return
        }

        ServicesStatus.MAIN_SERVICE.value = true
        val filter = IntentFilter().apply {
            addAction(ACTION_SCREEN_ON)
            addAction(ACTION_SCREEN_OFF)
        }
        if (!isReceiverRegistered) {
            registerReceiver(screenStateReceiver, filter)
            isReceiverRegistered = true
        }
        windowManager = getSystemService()!!
        val powerManager: PowerManager = getSystemService()!!
        val displayManager: DisplayManager = getSystemService()!!
        orientationListener = OrientationListener(this, windowManager!!, displayManager)
        orientationListener!!.onConfigurationChanged()
        if (powerManager.isInteractive) {
            orientationListener!!.enable()
        }
        trackerView = addTrackerOverlay(this, windowManager!!, orientationListener!!)
        super.onCreate()
    }

    override fun onDestroy() {
        trackerView?.let { removeOverlay(it, windowManager!!, false) }
        if (isReceiverRegistered) {
            unregisterReceiver(screenStateReceiver)
        }
        ServicesStatus.MAIN_SERVICE.value = false
    }

    override fun onBind(intent: Intent?): LocalBinder {
        return LocalBinder()
    }

    inner class LocalBinder : Binder() {
        fun updateRotationState(newState: RotationState) {
            orientationListener?.updateRotationState(newState)
        }
    }
}