/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.accessibilityservice.AccessibilityService
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.ActivityInfo.*
import android.content.pm.PackageManager
import android.os.IBinder
import android.util.Log
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityWindowInfo.TYPE_SPLIT_SCREEN_DIVIDER



class ActivityTrackerService : AccessibilityService() {
    private var requestedScreenOrientation: Int? = null
    private var trackerService: OrientationTrackerService.LocalBinder? = null
    private var wanted = false
    private var lastRotationState = RotationState.ALL

    override fun onServiceConnected() {
        ServicesStatus.ACCESSIBILITY_SERVICE.value = true
        super.onServiceConnected()
        wanted = true
        bindTrackerService()
    }

    fun bindTrackerService() {
        val intent = Intent(this, OrientationTrackerService::class.java)
        startService(intent)
        bindService(intent, serviceConnection, 0)
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            trackerService = service as OrientationTrackerService.LocalBinder
            trackerService?.updateRotationState(lastRotationState)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            trackerService = null
            if (wanted) {
                bindTrackerService()
            }
        }
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        if (event.eventType == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            lastRotationState = determineRotationState(event)
            trackerService?.updateRotationState(lastRotationState) ?: run {
                Log.w(TAG, "No tracker service available")
                bindTrackerService()
            }
        }
    }

    private fun determineRotationState(event: AccessibilityEvent): RotationState {
        if (windows.any { it.type == TYPE_SPLIT_SCREEN_DIVIDER }) {
            requestedScreenOrientation = null // split-screen can always rotate, regardless of manifest flags
        } else if (event.packageName != null && event.className != null) {
            /*
            The component is often actually just a plain old View, but that doesn't matter:
            we're looking for an activity with that name, and Java only allows single
            inheritance, therefore nothing can be both an Activity and a View, so false positives
            are impossible.
            */
            val component = ComponentName(event.packageName.toString(), event.className.toString())
            try {
                requestedScreenOrientation = packageManager.getActivityInfo(component, 0).screenOrientation
                Log.d(TAG, "Got orientation $requestedScreenOrientation from $component on $event")
            } catch (e: PackageManager.NameNotFoundException) {
                Log.v(TAG, "Exception getting running activity orientation flag on $event", e)
            }
        }

        return when (requestedScreenOrientation) {
            SCREEN_ORIENTATION_BEHIND -> RotationState.ALL
            SCREEN_ORIENTATION_FULL_SENSOR -> RotationState.NONE
            SCREEN_ORIENTATION_FULL_USER -> RotationState.ALL
            SCREEN_ORIENTATION_LANDSCAPE -> RotationState.NONE
            SCREEN_ORIENTATION_LOCKED -> RotationState.NONE
            SCREEN_ORIENTATION_NOSENSOR -> RotationState.NONE // "NOSENSOR means the display's "natural" orientation"
            SCREEN_ORIENTATION_PORTRAIT -> RotationState.NONE
            SCREEN_ORIENTATION_REVERSE_LANDSCAPE -> RotationState.NONE
            SCREEN_ORIENTATION_REVERSE_PORTRAIT -> RotationState.NONE
            SCREEN_ORIENTATION_SENSOR -> RotationState.NONE
            SCREEN_ORIENTATION_SENSOR_LANDSCAPE -> RotationState.NONE
            SCREEN_ORIENTATION_SENSOR_PORTRAIT -> RotationState.NONE
            SCREEN_ORIENTATION_UNSPECIFIED -> RotationState.ALL
            SCREEN_ORIENTATION_USER -> RotationState.ALL
            SCREEN_ORIENTATION_USER_LANDSCAPE -> RotationState.LANDSCAPE_ONLY
            SCREEN_ORIENTATION_USER_PORTRAIT -> RotationState.PORTRAIT_ONLY
            null -> RotationState.ALL
            else -> {
                Log.e(TAG, "Unknown orientation $requestedScreenOrientation")
                RotationState.ALL
            }
        }
    }

    override fun onInterrupt() {
        Log.e(TAG, "onInterrupt() called")
        ServicesStatus.ACCESSIBILITY_SERVICE.value = false
        wanted = false
        unbindService(serviceConnection)
    }
}

private const val TAG = "ActivityTrackerService"