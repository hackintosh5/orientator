/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

val Context.bootReceiverData by preferencesDataStore(name = "boot_receiver")
val LAST_BOOT_RECEIVER_RUN_TIME = longPreferencesKey("last_boot_receiver_run_time")

class BootReceiver : BroadcastReceiver() {
    @OptIn(DelicateCoroutinesApi::class)
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != "android.intent.action.BOOT_COMPLETED" && intent.action != "android.intent.action.ACTION_BOOT_COMPLETED" && intent.action != "android.intent.action.QUICKBOOT_POWERON" && intent.action != "com.htc.intent.action.QUICKBOOT_POWERON") {
            return
        }
        GlobalScope.launch {
            context.bootReceiverData.edit { settings ->
                settings[LAST_BOOT_RECEIVER_RUN_TIME] = System.currentTimeMillis()
            }
        }
        context.startService(Intent(context, OrientationTrackerService::class.java))
    }
}
