/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.datastore.preferences.core.edit
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.entity.*
import com.mikepenz.aboutlibraries.ui.compose.Libraries
import com.mikepenz.aboutlibraries.util.withContext
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.launch

sealed class Page(val route: String, @StringRes val label: Int, val icon: ImageVector, val content: @Composable (NavBackStackEntry) -> Unit) {
    @OptIn(ExperimentalMaterialApi::class)
    object Status : Page("status", R.string.page_status_label, Icons.Filled.Info, {
        val context = LocalContext.current
        LaunchedEffect(context) { updateStatus(context) }
        val status = getStatus()

        Column(Modifier.verticalScroll(rememberScrollState())) {
            status.forEach { entry ->
                val statusIcon = if (entry.status) Icons.Default.CheckCircleOutline else Icons.Default.ErrorOutline
                val statusDescription = stringResource(if (entry.status) R.string.status_okay else R.string.status_not_okay)
                val statusTint = colorResource(if (entry.status) {
                    R.color.status_okay
                } else {
                    when (entry.importance) {
                        Importance.REQUIRED -> R.color.status_not_okay_required
                        Importance.OPTIONAL -> R.color.status_not_okay_optional
                    }
                })

                Box(
                    modifier = Modifier
                        .wrapContentHeight()
                        .fillMaxWidth()
                        .clickable {
                            if (!entry.status) {
                                entry.onClick?.invoke(context)
                            }
                        }
                        .padding(16.dp),
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Start
                    ) {
                        Icon(
                            entry.icon,
                            contentDescription = null,
                            modifier = Modifier.size(48.dp),
                            tint = MaterialTheme.colors.onBackground
                        )
                        Column(
                            modifier = Modifier
                                .wrapContentHeight()
                                .fillMaxWidth()
                        ) {
                            Row(horizontalArrangement = Arrangement.SpaceBetween) {
                                Text(
                                    stringResource(entry.title),
                                    style = MaterialTheme.typography.h6,
                                    color = MaterialTheme.colors.onBackground,
                                    modifier = Modifier.weight(1f)
                                )
                                Spacer(Modifier.width(8.dp))
                                Icon(
                                    statusIcon,
                                    contentDescription = statusDescription,
                                    tint = statusTint,
                                    modifier = Modifier
                                        .size(32.dp)
                                )
                            }
                            Text(stringResource(entry.description), color = MaterialTheme.colors.onBackground)
                        }
                    }
                }
            }
        }
    })
    object Licenses : Page("licenses", R.string.page_licenses_label, Icons.Filled.Star, {
        val libraries = remember { mutableStateOf<Libs?>(null) }

        val context = LocalContext.current
        LaunchedEffect(libraries) {
            libraries.value = Libs.Builder().withContext(context).build()
        }

        val thisLibrary = Library(
            "dev.hack5:orientator",
            BuildConfig.VERSION_NAME,
            stringResource(R.string.app_name),
            stringResource(R.string.this_app),
            "https://hack5.dev/about/projects/Orientator",
            listOf(Developer("hackintosh5", "https://hack5.dev/about")),
            null,
            Scm(
                "scm:git:https://hack5.dev/about/projects/Orientator",
                "scm:git:ssh://git@gitlab.com/hackintosh5/orientator.git",
                "https://gitlab.com/hackintosh5/orientator.git"
            ),
            setOf(
                License(
                    SpdxLicense.GPL_3_0_or_later.fullName,
                    SpdxLicense.GPL_3_0_or_later.getUrl(),
                    null,
                    SpdxLicense.GPL_3_0_or_later.id,
                context.resources.openRawResource(R.raw.license).bufferedReader().readText(),
                SpdxLicense.GPL_3_0_or_later.id
            )),
            setOf(Funding("PayPal", "https://paypal.me/hackintosh5")),
            null
        )
        val libs = libraries.value?.libraries
        if (libs != null) {
            Libraries(
                libraries = listOf(thisLibrary) + libs,
                Modifier.fillMaxSize()
            )
        }
    })
    object Settings : Page("settings", R.string.page_settings_label, Icons.Filled.Settings, {
        val settingsData by LocalContext.current.settingsData.data.collectAsState(null)

        LoadableView {
            waitFor(settingsData != null)

            whenReady {
                var showDialog: SettingsEntry<*>? by remember { mutableStateOf(null) }
                SettingsDialog(showDialog) { showDialog = it }

                Column(Modifier.verticalScroll(rememberScrollState())) {
                    getSettings(settingsData!!).forEach { entry ->

                        Box(
                            modifier = Modifier
                                .wrapContentHeight()
                                .fillMaxWidth()
                                .clickable {
                                    showDialog = entry
                                }
                                .padding(16.dp),
                        ) {
                            Column(
                                modifier = Modifier
                                    .wrapContentHeight()
                                    .fillMaxWidth()
                            ) {
                                Text(
                                    stringResource(entry.title),
                                    style = MaterialTheme.typography.h6,
                                    color = MaterialTheme.colors.onBackground
                                )
                                Text(
                                    stringResource(entry.description),
                                    color = MaterialTheme.colors.onBackground
                                )
                            }
                        }
                    }
                }
            }
        }
    })
}

val ALL_PAGES = listOf(
    Page.Status, Page.Licenses, Page.Settings,
)


@Composable
fun <T>SettingsDialog(showDialog: SettingsEntry<T>?, setShowDialog: (SettingsEntry<T>?) -> Unit) {
    showDialog?.let {
        val scope = rememberCoroutineScope()
        val localContext by rememberUpdatedState(LocalContext.current)
        var rawValue by remember { mutableStateOf(it.value.toString()) }
        var value by remember { mutableStateOf(true to it.value) }
        AlertDialog(
            onDismissRequest = { setShowDialog(null) },
            title = {
                Text(
                    stringResource(it.title),
                    style = MaterialTheme.typography.h6
                )
            },
            text = {
                Column {
                    Text(stringResource(it.description))
                    when (it.value) {
                        is SettingsEntryData.StringData -> {
                            OutlinedTextField(
                                rawValue,
                                {
                                    rawValue = it
                                    @Suppress("UNCHECKED_CAST")
                                    value = true to (SettingsEntryData.StringData(it.ifEmpty { null }) as SettingsEntryData<T>)
                                }
                            )
                        }
                        is SettingsEntryData.IntData -> {
                            OutlinedTextField(
                                rawValue,
                                {
                                    rawValue = it
                                    @Suppress("UNCHECKED_CAST")
                                    value = (it.isEmpty() || it.toIntOrNull() != null) to (SettingsEntryData.IntData(it.toIntOrNull()) as SettingsEntryData<T>)
                                }
                            )
                        }
                        is SettingsEntryData.LongData -> {
                            OutlinedTextField(
                                rawValue,
                                {
                                    rawValue = it
                                    @Suppress("UNCHECKED_CAST")
                                    value = (it.isEmpty() || it.toLongOrNull() != null) to (SettingsEntryData.LongData(it.toLongOrNull()) as SettingsEntryData<T>)
                                }
                            )
                        }
                    }
                }
            },
            confirmButton = {
                Button(
                    enabled = value.first,
                    onClick = {
                        scope.launch(NonCancellable) {
                            localContext.settingsData.edit { preferences ->
                                if (value.second.value == null) {
                                    preferences.remove(it.key)
                                } else {
                                    preferences[it.key] = value.second.value!!
                                }
                            }
                            setShowDialog(null)
                        }
                    }
                ) {
                    Text(stringResource(R.string.save_setting_change))
                }
            },
            dismissButton = {
                Button(onClick = { setShowDialog(null) }) {
                    Text(stringResource(R.string.cancel_setting_change))
                }
            }
        )
    }
}


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            OrientatorTheme {
                val navController = rememberNavController()

                AppScaffold(navController) {
                    NavHost(navController, startDestination = Page.Status.route) {
                        ALL_PAGES.forEach { page ->
                            composable(page.route) {
                                page.content(it)
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        updateStatus(this)
        super.onResume()
    }
}

@Composable
fun AppScaffold(navController: NavController, content: @Composable () -> Unit) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(stringResource(R.string.app_name)) }
            )
        },
        bottomBar = {
            BottomNavigation(Modifier.wrapContentHeight()) {
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                ALL_PAGES.forEach { page ->
                    BottomNavigationItem(
                        icon = { Icon(page.icon, contentDescription = null) },
                        label = { Text(stringResource(page.label)) },
                        alwaysShowLabel = true,
                        selected = navBackStackEntry?.destination?.route == page.route,
                        onClick = {
                            navController.navigate(page.route) {
                                popUpTo(navController.graph.startDestinationId) {
                                    inclusive = false
                                    saveState = false
                                }
                                launchSingleTop = true
                                restoreState = false
                            }
                        }
                    )
                }
            }
        }
    ) { contentPadding ->
        Box(modifier = Modifier.padding(contentPadding)) {
            content()
        }
    }
}

@Composable
fun OrientatorTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        darkColors()
    } else {
        lightColors()
    }

    MaterialTheme(colors = colors, content = content)
}
