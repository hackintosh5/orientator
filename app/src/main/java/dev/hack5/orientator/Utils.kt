/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.content.Context
import android.net.Uri


enum class RotationState {
    LANDSCAPE_ONLY,
    PORTRAIT_ONLY,
    ALL,
    NONE,
}

fun statusList(
    serviceRunning: Boolean,
    canWriteSettings: Boolean,
    canDrawOverlays: Boolean,
    accessibilityServiceRunning: Boolean,
    bootReceiverCalled: Boolean
) = listOf(
    StatusEntry.ServiceRunning(serviceRunning),
    StatusEntry.CanWriteSettings(canWriteSettings),
    StatusEntry.CanDrawOverlays(canDrawOverlays),
    StatusEntry.AccessibilityServiceRunning(accessibilityServiceRunning),
    StatusEntry.BootReceiverCalled(bootReceiverCalled)
)

enum class Importance {
    REQUIRED,
    OPTIONAL,
}

val Context.packageUri: Uri get() = Uri.fromParts("package", packageName, null)
