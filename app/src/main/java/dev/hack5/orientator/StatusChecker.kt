/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.SystemClock
import android.provider.Settings
import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.vector.ImageVector
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.map

sealed class StatusEntry(@StringRes val title: Int, @StringRes val description: Int, val icon: ImageVector, val importance: Importance, val status: Boolean, val onClick: (Context.() -> Unit)?) {
    class ServiceRunning(status: Boolean) : StatusEntry(R.string.status_service_running, R.string.status_service_running_desc, Icons.Default.MiscellaneousServices, Importance.REQUIRED, status, {
        startService(Intent(this, OrientationTrackerService::class.java))
    })
    class CanWriteSettings(status: Boolean) : StatusEntry(R.string.status_can_write_settings, R.string.status_can_write_settings_desc, Icons.Default.AppSettingsAlt, Importance.REQUIRED, status, if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {{
        startActivity(Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, packageUri))
    }} else null)
    class CanDrawOverlays(status: Boolean) : StatusEntry(R.string.status_can_draw_overlays, R.string.status_can_draw_overlays_desc, Icons.Default.Layers, Importance.REQUIRED, status, if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {{
        startActivity(Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, packageUri))
    }} else null)
    class AccessibilityServiceRunning(status: Boolean) : StatusEntry(R.string.status_accessibility_service_running, R.string.status_accessibility_service_running_desc, Icons.Default.SettingsAccessibility, Importance.OPTIONAL, status, {
        startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS))
    })
    class BootReceiverCalled(status: Boolean) : StatusEntry(R.string.status_boot_receiver_called, R.string.status_boot_receiver_called_desc, /*Icons.Default.Start*/Icons.Default.PlayArrow, Importance.OPTIONAL, status, null)
}

fun updateStatus(context: Context) {
    ServicesStatus.CAN_WRITE_SETTINGS.value = Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.System.canWrite(context)
    ServicesStatus.CAN_DRAW_OVERLAYS.value = Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(context)
    ServicesStatus.LAST_BOOT_RECEIVER_RUN_TIME.value = context.bootReceiverData.data.map { preferences -> preferences[LAST_BOOT_RECEIVER_RUN_TIME] ?: 0L }
}

@Composable
fun getStatus(): List<StatusEntry> {
    val serviceRunning by ServicesStatus.MAIN_SERVICE
    val canWriteSettings by ServicesStatus.CAN_WRITE_SETTINGS
    val canDrawOverlays by ServicesStatus.CAN_DRAW_OVERLAYS
    val accessibilityServiceRunning by ServicesStatus.ACCESSIBILITY_SERVICE
    val lastBootReceiverRunTimeFlow by ServicesStatus.LAST_BOOT_RECEIVER_RUN_TIME
    val lastBootReceiverRunTime by lastBootReceiverRunTimeFlow.collectAsState(0L)
    return statusList(
        serviceRunning,
        canWriteSettings,
        canDrawOverlays,
        accessibilityServiceRunning,
        lastBootReceiverRunTime > (System.currentTimeMillis() - SystemClock.elapsedRealtime())
    )
}


object ServicesStatus {
    val MAIN_SERVICE = mutableStateOf(false)
    val CAN_WRITE_SETTINGS = mutableStateOf(false)
    val CAN_DRAW_OVERLAYS = mutableStateOf(false)
    val ACCESSIBILITY_SERVICE = mutableStateOf(false)
    val LAST_BOOT_RECEIVER_RUN_TIME: MutableState<Flow<Long>> = mutableStateOf(MutableSharedFlow())
}
