/*
 * Orientator
 * Copyright (C) 2022 Hackintosh Five
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */

package dev.hack5.orientator

import android.content.Context
import androidx.annotation.StringRes
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.DEBUG_PROPERTY_VALUE_AUTO

val Context.settingsData by preferencesDataStore(name = "settings")
val CUSTOM_ROTATION = intPreferencesKey("custom_rotation")
val CUSTOM_CLICKER_OFFSET_X = intPreferencesKey("custom_clicker_offset_x")
val CUSTOM_CLICKER_OFFSET_Y = intPreferencesKey("custom_clicker_offset_y")
val CUSTOM_CLICKER_DIAMETER = intPreferencesKey("custom_clicker_diameter")
val HIDE_DELAY = longPreferencesKey("hide_delay")
val DEBOUNCE_INTERVAL = longPreferencesKey("debounce_interval")


fun getSettings(settingsData: Preferences): List<SettingsEntry<*>> {
    return listOf(
        SettingsEntry(
            R.string.setting_custom_rotation,
            R.string.setting_custom_rotation_desc,
            SettingsEntryData.IntData(settingsData[CUSTOM_ROTATION]),
            CUSTOM_ROTATION
        ),
        SettingsEntry(
            R.string.setting_custom_clicker_offset_x,
            R.string.setting_custom_clicker_offset_desc,
            SettingsEntryData.IntData(settingsData[CUSTOM_CLICKER_OFFSET_X]),
            CUSTOM_CLICKER_OFFSET_X
        ),
        SettingsEntry(
            R.string.setting_custom_clicker_offset_y,
            R.string.setting_custom_clicker_offset_desc,
            SettingsEntryData.IntData(settingsData[CUSTOM_CLICKER_OFFSET_Y]),
            CUSTOM_CLICKER_OFFSET_Y
        ),
        SettingsEntry(
            R.string.setting_custom_clicker_diameter,
            R.string.setting_custom_clicker_diameter_desc,
            SettingsEntryData.IntData(settingsData[CUSTOM_CLICKER_DIAMETER]),
            CUSTOM_CLICKER_DIAMETER
        ),
        SettingsEntry(
            R.string.setting_hide_delay,
            R.string.setting_hide_delay_desc,
            SettingsEntryData.LongData(settingsData[HIDE_DELAY]),
            HIDE_DELAY
        ),
        SettingsEntry(
            R.string.setting_debounce_interval,
            R.string.setting_debounce_interval_desc,
            SettingsEntryData.LongData(settingsData[DEBOUNCE_INTERVAL]),
            DEBOUNCE_INTERVAL
        ),
    )
}

data class SettingsEntry<T>(@StringRes val title: Int, @StringRes val description: Int, val value: SettingsEntryData<T>, val key: Preferences.Key<T>)

sealed class SettingsEntryData<T>(val value: T?) {
    class StringData(value: String?) : SettingsEntryData<String>(value)
    class IntData(value: Int?) : SettingsEntryData<Int>(value)
    class LongData(value: Long?) : SettingsEntryData<Long>(value)

    override fun toString() = value?.toString() ?: ""
}